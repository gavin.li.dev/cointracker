import * as React from "react";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import './App.css';
// import { useEffect } from 'react';

// import { useObserver } from "mobx-react-lite";
import { getRootStore } from './store';
import { Navbar } from './components';

import Home from './views/home';
import Login from './views/login';
import { CoinListView } from './views/coin';

function App() {
  // useEffect(() => {
    
  // }, []);

  // function handleGoogleResponse(res) {
  //   console.log(res.credential);
  // };


  // const { coinStore, userStore }  = useStore();
  // const [ coin, setCoin ] = React.useState("");

  const userStore = getRootStore().getUserStore();

  // const addCoin = () => {
  //   coinStore.addCoin({
  //     name: 'newCoin'
  //   })
  // }

  // const handleNameChange = (e) => {
  //   e.preventDefault();

  //   const v = e.target.value;

  //   userStore.setName(v);

  // }

  // const handleCoinChange = (e) => {

  // }

  return (
    <div className="App">
      
      <BrowserRouter>
        <Navbar />

        <h1>Coin Tracker: {userStore.name} - {userStore.email}</h1>

        <Routes>
          <Route path="/" element={<Home site="waht a site"/>} />
          <Route path="/login" element={<Login />} />
          <Route path="/coins" element={<CoinListView />} />
        </Routes>

      </BrowserRouter>
    </div>
  );
}

export default App;
