import { useState } from 'react';
import { observer } from 'mobx-react-lite';

const TV = () => {
    const [qty, setQty] = useState(0);
    const [pri, setPri] = useState(0);

    const updateQty = (q) => {
        // console.log(q.target.value);
        setQty(q.target.value)
    }
    
    const updatePri = (p) => {
        // console.log(p.target.value);
        setPri(p.target.value)
    }

    return (
    <>
        <div>HO xpx OH</div>
        <label>
            Quantity:
            <input type="text" value={qty} onChange={updateQty} />
        </label>
        <label>
            Price:
            <input type="text" value={pri} onChange={updatePri} /> (USD)
        </label>
        <button>Add to Portfolio</button>
    </>
)};

const CoinListView = observer(() => {
    const coins = [
        "btc",
        "eth",
        "xrp",
        "bnb",
        "litecoin"
    ]


    return (
        <div>
            <TV ></TV>
            {coins.map(c => {
                return <div key={c}>{c}</div>
            })}
        </div>
    )
})

export default CoinListView;