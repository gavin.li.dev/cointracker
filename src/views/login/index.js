import { useEffect, useState } from "react";
import { observer } from 'mobx-react-lite';
import axios from 'axios';
// import UserStore from '../../store/User.store';
import { getRootStore } from '../../store';
import jwt_decode from 'jwt-decode';

let userStore, uiStore;

/* global google */
const handleGoogleResponse = async(res) => {
    // console.log(res.credential);

    try {
        u = jwt_decode(res.credential);
        // console.log(u);
        // store.user = u;


        userStore.login({
            email: u.email,
            name: u.name,
            avatar: u.picture,
            verified: u.email_verified,
            googleId: u.sub
        });

        const authResult = await axios.post(
            'http://localhost:8080/api/user/auth',
            { credential: res.credential }
        )

        console.log(authResult);

    } catch(err) {
        console.log('token modified');
        throw err;
    }
}

const initGoogleAuthButton = () => {
    google.accounts.id.renderButton(
        document.getElementById('google-login-button'),
        { theme: "outline", size: "large" }
    );
}

const Login = observer(() => {
    const rootStore = getRootStore();
    userStore = rootStore.getUserStore();
    uiStore = rootStore.getUiStore();

    console.log('login view rendered!!');
    // console.log(userStore);
 
    let scriptLoaded = uiStore.getScriptLoaded('googleAuth');

    useEffect(() => {
        console.log('try use effect');
        if (scriptLoaded) {
            console.log('script loaded!!');
            initGoogleAuthButton();
        } else  {
            uiStore.setScriptLoaded('googleAuth', true);

            const script = document.createElement('script');
            script.src = "https://accounts.google.com/gsi/client";
            script.async = true;
            script.defer = true;
            
            script.onload = () => {
                google.accounts.id.initialize({
                    client_id: "761917884177-et3gvn7b1rsk37okcqlotsbk6cloas5d.apps.googleusercontent.com",
                    callback: handleGoogleResponse
                });

                initGoogleAuthButton();
            }

            document.head.appendChild(script);
        }

        return;
      }, []);
    return (
        <>
            <h2>Login from here</h2>
            {userStore.isLogin
                ? <div>Hello {userStore.name}</div>
                : <div id="google-login-button"></div>
            }
        </>
    );
});

export default Login;