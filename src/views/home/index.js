import * as React from 'react';

function Home(props) {
  console.log('home view rendered!')
    return (
      <>
        <main>
          <h2>Hello, {props.site}</h2>
        </main>
      </>
    )
  }

export default Home;