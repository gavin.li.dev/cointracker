import { makeAutoObservable } from 'mobx';
import { makePersistable } from 'mobx-persist-store';

class UserStore {
    isLogin = false;
    name = "john";
    email = "john@doe.com";
    uid = "123";

    constructor() {
        makeAutoObservable(this);
        makePersistable(this, {
            name: "UserStore",
            properties: ["isLogin", "name", "email", "uid"],
            storage: window.localStorage
        });
    }

    setName(name) {
        this.name = name;
    }

    setEmail(email) {
        this.email = email;
    }

    setUid(uid) {
        this.uid = uid;
    }

    login(u) {
        this.isLogin = true;
        this.name = u.name;
        this.email = u.email;
    }

    isLogin() {
        return this.isLogin;
    }
}

export default UserStore;