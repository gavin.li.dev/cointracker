import { makeAutoObservable } from "mobx";

class Coin {
    name;
}

class CoinStore {
    rs;
    coins = [];

    constructor(rs) {
        makeAutoObservable(this);
        this.rs = rs;
    }

    addCoin(coin) {
        this.coins.push(coin);
    }
}

export default CoinStore;