import { makeAutoObservable } from "mobx";

export default class UiStore {
    extScriptsLoaded = {
        googleAuth: false
    }

    constructor() {
        makeAutoObservable(this);
    }

    setScriptLoaded(key, value) {
        this.extScriptsLoaded[key] = value;
    }

    getScriptLoaded(key) {
        return this.extScriptsLoaded[key];
    }
}