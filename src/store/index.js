// import { configureStore } from "@reduxjs/toolkit";
import react from "react";
import CoinStore from './coin';
import UserStore from './User.store';
import UiStore from "./Ui.store";

class RootStore {
    coinStore;
    userStore;
    uiStore;

    constructor() {
        console.log('root store constructed!');
        this.userStore = new UserStore(this);
        this.coinStore = new CoinStore(this);
        this.uiStore = new UiStore();
    }

    getUserStore() {
        return this.userStore;
    }

    getCoinStore() {
        return this.coinStore;
    }

    getUiStore() {
        return this.uiStore;
    }
}

const StoresContext = react.createContext(new RootStore());

export const getRootStore = () => react.useContext(StoresContext);