import React from 'react';
import { Link } from 'react-router-dom';
import { observer } from 'mobx-react-lite';
import { getRootStore } from '../store';

import styles from './navbar.module.css';

const Navbar = observer(() => {
    const userStore = getRootStore().getUserStore();

    // console.log(user);

    return (
        <div className={styles.navbar}>
            <ul className={styles['nav']}>
                <li>
                    <Link to="/">HOME</Link>
                </li>
                <li>
                    <Link to="/coins">Coins</Link>
                </li>
                <li>
                    <Link to="/create-post">Write Blog</Link>
                </li>
            </ul>

            <div className={styles.username}>
                {userStore.isLogin
                    ? 'hello' + userStore.name
                    : <Link to="/login">Login</Link>
                }
            </div>
        </div>
    )
})

export default Navbar;